from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.views.generic import ListView
from django.utils import timezone

from models import Entry
import views

urlpatterns = patterns('',
                       url(r'^$',
                           ListView.as_view(
            queryset=Entry.objects.order_by('-date'),
            context_object_name='entries',
            template_name="microblog/list.html"
            ),
                           name="entry_list"
                           ),
                       url(r'^add/',
                           TemplateView.as_view(
            queryset=timezone.now(),
            context_object_name='date'
            template_name="microblog/add.html"
            ),
                           name="add"
                           )
                       )
