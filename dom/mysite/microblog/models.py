from django.db import models
from django.utils import timezone

class Entry(models.Model):
    author=models.CharField(max_length=50)
    text=models.CharField(max_length=200)
    date = models.DateTimeField('termin publikacji')

    def __unicode__(self):
        return self.author+": "+str(self.date)+" -- "+self.text
